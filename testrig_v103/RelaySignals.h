#ifndef RelaySignals_H
#define RelaySignals_H

#if (ARDUINO >= 100)
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

class RelaySignals {

  public:
    //Variables
    
    //Constructor
    RelaySignals(bool binVal=false);
    
    //Methods
    void begin();
    bool isOnRelay(uint8_t index);
  
  private:
    //Variables
    bool _msgState;
    
    uint8_t _pins[10] = {
      A4, A5, 2, 3, 4, 5, 6, 7, 8, 9
    };
    
    //Methods
    void _displayMsg(String msg);
  
};


#endif

