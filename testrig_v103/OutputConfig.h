#ifndef OutputConfig_H
#define OutputConfig_H

#if (ARDUINO >= 100)
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

#include <SPI.h>
#include "RegAddrConst.h"
#include "ChipSelects.h"

class OutputConfig {

  public:
    //Variables
    
    //Constructor
    OutputConfig(bool binVal=false);
  
    //Methods
    bool begin(ChipSelects cs, bool chkstate=false, unsigned long speed=SPI_CLOCK_DIV2);
    void transactMcpSPI(bool start);
    void setEnableOutPort(uint8_t portNum);
    void setRegisterConfig(String addr, uint8_t data);
  
  private:
    //Variables
    bool _msgState;
    unsigned long _speed;
    
    //Methods
    void _displayMsg(String msg);
    void _transferSPI(uint8_t head, uint8_t addr, uint8_t data);
    SPISettings _spiSet();
    
};


#endif

