#include "DigiPotConfig.h"

DigiPotConfig::DigiPotConfig(bool binVal) { // turns on the displaying of serial print outs
  _msgState = binVal;
  
}

bool DigiPotConfig::begin(ChipSelects cs, bool chkstate, uint32_t speed) { // setup all digipot (AD5160) up to configuration close by default on voltage output as voltage dividers.
  _displayMsg("...SPI DigiPot Init!");

  _cs = cs; // chipselect object/class to access ChipSelects' methods
  _speed = speed; // setup the speed of the SPI bus transaction between digipot (AD5160)

  if (!chkstate) { // to remove redundancy of declaring SPI all over again
    SPI.begin();
    chkstate = true;
  }
  
  _displayMsg("...SPI DigiPot: Active!");

  return chkstate; // returns status of SPI declaration
}

void DigiPotConfig::setDefaultAll(uint8_t mode, uint8_t state) {
  
  transactDigiPotSPI(1);
  for (uint8_t i = 1; i < 11; i++) {  // cause of 10 digital potentiometer
    _cs.csMainDigiPot(i); // enables all digital potentiometer to be set on either 13.3mV or 133mV output config at main board
        
    switch (mode) {
      case 0:
        if (state == MINI) {
          setWiperValueWB(10);  // divisible of 10 results to 13.3mV         
        } else {
          setWiperValueWB(1);   // divisible of 1 results to 133mV
        }
        break;
        
      case 1:
        if (state == MINI) {
          setWiperValueWA(10);          
        } else {
          setWiperValueWA(1);
        }
        break;
              
      case 2:
        if (state == MINI) {
          setWiperValue(10);          
        } else {
          setWiperValue(1);
        }
    }

    _cs.csOff();
    delay(10);
    
  }

  for (uint8_t i = 1; i < 11; i++) {  // cause of 10 digital potentiometer
    _cs.csIsoDigiPot(i); // enables all digital potentiometer to be set on either 13.3mV or 133mV output config at main board
        
    switch (mode) {
      case 0:
        if (state == MINI) {
          setWiperValueWB(10);  // divisible of 10 results to 13.3mV           
        } else {
          setWiperValueWB(1);   // divisible of 1 results to 133mV
        }
        break;
        
      case 1:
        if (state == MINI) {
          setWiperValueWA(10);          
        } else {
          setWiperValueWA(1);
        }
        break;
              
      case 2:
        if (state == MINI) {
          setWiperValue(10);          
        } else {
          setWiperValue(1);
        }
    }

    _cs.csOff();
    delay(10);
    
  }
  
  transactDigiPotSPI(0);
  
}

void DigiPotConfig::setWiperValueWB(uint8_t divisible) {

  switch (divisible) {  //Rheostat Rwb setup AD5160.. 5k as R2 and 5M as R1 on voltage divider setup (see v103) 5M
    case 1:
      SPI.transfer(0x98); // 133.0 mV, 152 steps, 3028.750 ohms
      break;
      
    case 2:
      SPI.transfer(0x4B); // 67.0 mV, 75 steps, 1524.844 ohms
      break;
      
    case 3:
      SPI.transfer(0x31); // 44.4 mV, 49 steps, 1017.031 ohms
      break;
      
    case 4:
      SPI.transfer(0x24); // 33.3 mV, 36 steps, 763.125 ohms
      break;
      
    case 5:
      SPI.transfer(0x1C); // 26.7 mV, 28 steps, 606.875 ohms
      break;
      
    case 6:
      SPI.transfer(0x17); // 22.2 mV, 23 steps, 509.219 ohms
      break;
      
    case 7:
      SPI.transfer(0x13); // 19.0 mV, 19 steps, 431.094 ohms
      break;
      
    case 8:
      SPI.transfer(0x10); // 16.7 mV, 16 steps, 372.500 ohms
      break;
      
    case 9:
      SPI.transfer(0x0E); // 14.8 mV, 14 steps, 333.438 ohms
      break;
      
    case 10:
      SPI.transfer(0x0C); // 13.3 mV, 12 steps, 294.375 ohms
      break;
      
  }
}

void DigiPotConfig::setWiperValueWA(uint8_t divisible) {

  switch (divisible) {  //Rheostat Rwa setup AD5160.. 5k as R2 and 5M as R1 on voltage divider setup (see v103) 5M
    case 1:
      SPI.transfer(0xFF); // 133.0 mV, 255 steps, 3028.750 ohms
      break;
      
    case 2:
      SPI.transfer(0x81); // 67.0 mV, 129 steps, 1524.844 ohms
      break;
      
    case 3:
      SPI.transfer(0x56); // 44.4 mV, 207 steps, 1017.031 ohms
      break;
      
    case 4:
      SPI.transfer(0x40); // 33.3 mV, 220 steps, 763.125 ohms
      break;
      
    case 5:
      SPI.transfer(0x34); // 26.7 mV, 228 steps, 606.875 ohms
      break;
      
    case 6:
      SPI.transfer(0x2B); // 22.2 mV, 233 steps, 509.219 ohms
      break;
      
    case 7:
      SPI.transfer(0x25); // 19.0 mV, 237 steps, 431.094 ohms
      break;
      
    case 8:
      SPI.transfer(0x20); // 16.7 mV, 240 steps, 372.500 ohms
      break;
      
    case 9:
      SPI.transfer(0x1D); // 14.8 mV, 242 steps, 333.438 ohms
      break;
      
    case 10:
      SPI.transfer(0x1A); // 13.3 mV, 244 steps, 294.375 ohms
      break;
      
  }
}

void DigiPotConfig::setWiperValue(uint8_t divisible) {

  switch (divisible) {  //potentiometer setup AD5160.. 5k as R2 and 5M as R1 on voltage divider setup (see v104) 8.3M, Rb GND connected
    case 1:
      SPI.transfer(0x68); // 133.0 mV, 20 steps, 4500:502 ohms
      break;
      
    case 2:
      SPI.transfer(0xB5); // 67.0 mV, 181 steps, 1524.844 ohms
      break;
      
    case 3:
      SPI.transfer(0xCF); // 44.4 mV, 207 steps, 1017.031 ohms
      break;
      
    case 4:
      SPI.transfer(0xDC); // 33.3 mV, 220 steps, 763.125 ohms
      break;
      
    case 5:
      SPI.transfer(0xE4); // 26.7 mV, 228 steps, 606.875 ohms
      break;
      
    case 6:
      SPI.transfer(0xE9); // 22.2 mV, 233 steps, 509.219 ohms
      break;
      
    case 7:
      SPI.transfer(0xED); // 19.0 mV, 237 steps, 431.094 ohms
      break;
      
    case 8:
      SPI.transfer(0xF0); // 16.7 mV, 240 steps, 372.500 ohms
      break;
      
    case 9:
      SPI.transfer(0xF2); // 14.8 mV, 242 steps, 333.438 ohms
      break;
      
    case 10:
      SPI.transfer(0xF4); // 13.3 mV, 244 steps, 294.375 ohms
      break;
      
  }
}

void DigiPotConfig::transactDigiPotSPI(bool start) {  // digipot AD5160 enabling/disabling spi transactions
  if (start) {
    SPI.beginTransaction(_spiSet());    
  } else {
    SPI.endTransaction();    
  }
}

SPISettings DigiPotConfig::_spiSet() { // method configuring spi settings such as speeds, polarity, phase and data transfer orientation
  SPISettings spi(_speed, MSBFIRST, SPI_MODE0);
  return spi;
}

void DigiPotConfig::_displayMsg(String msg) { // displays the serial print outs to determine what happened along the process
  if (Serial && _msgState) {
    Serial.println(msg);  
  }
}



