#include "OutputConfig.h"
#include "ChipSelects.h"
#include "RegAddrConst.h"
#include <SPI.h>

#define OPCODE_WRITE 0x40
#define OPCODE_READ 0x41

OutputConfig::OutputConfig(bool binVal) { // turns on the displaying of serial print outs
  _msgState = binVal;
  
}

bool OutputConfig::begin(ChipSelects cs, bool chkstate, unsigned long speed) {  // setup all mcp23S17t i/o expanders up to configuration close by default on their i/os.
  _displayMsg("...SPI All I/O Init!");

  _speed = speed; // setup the speed of the SPI bus transaction between mcp23S17t i/o expanders

  if (!chkstate) {  // to remove redundancy of declaring SPI all over again
    SPI.begin();
    chkstate = true;
  }

  // MCP setups
  RegAddrConst ra;

  byte iochg = 0;

  transactMcpSPI(1);  //  start of SPI transaction for mcp23S17t i/o expanders
  
  for (byte a = 1; a < 4; a++) {
    for (byte b = 1; b < 4; b++) {
        switch (b) {
          case 1:
            cs.csMainOutEn();    // MCP mainboard cs setup 
            break;
            
          case 2:
            cs.csIsoOutEn();     // MCP isolated board cs setup 
            break;
            
          case 3:
            cs.csMainPhaChg();   // MCP phase changer cs setup @ mainboard 
            break;
            
        }
        
        if (iochg == 0) {
          _transferSPI(ra.getOpcode(), ra.getAddrVal("IOCON1", 1), 0x20); // same bank, disable seqaddr, disable hw
          cs.csOff();        
        } else if (iochg == 1) {
          _transferSPI(ra.getOpcode(), ra.getAddrVal("IODIRA", 0), 0x00); // same bank, disable seqaddr, disable hw
          cs.csOff();            
        } else {
          _transferSPI(ra.getOpcode(), ra.getAddrVal("IODIRB", 0), 0x00); // same bank, disable seqaddr, disable hw
          cs.csOff();          
        }
    }
    iochg++;
  }
  
  //turn off all io ports
  cs.csMainOutEn();
  _transferSPI(ra.getOpcode(), ra.getAddrVal("GPIOA", 0), 0x00);
  cs.csOff();
  delay(2);
  cs.csIsoOutEn();
  _transferSPI(ra.getOpcode(), ra.getAddrVal("GPIOB", 0), 0x00);
  cs.csOff();
  delay(2);
  
  transactMcpSPI(0);  //  end of SPI transaction for mcp23S17t i/o expanders
  
  _displayMsg("...SPI I/Os: Active!"); 

  return chkstate; // returns status of SPI declaration
}

void OutputConfig::setEnableOutPort(uint8_t portNum) {  // set i/o expander's specific i/o port to be active high (to turn on analog switches)
  String gpio = "GPIOA";
  if (portNum > 8) {
    gpio = "GPIOB";
    portNum = portNum - 8;
  }
  switch (portNum) {
    case 1:
      portNum = 0x01; break;
    case 2:
      portNum = 0x02; break;
    case 3:
      portNum = 0x04; break;
    case 4:
      portNum = 0x08; break;
    case 5:
      portNum = 0x10; break;
    case 6:
      portNum = 0x20; break;
    case 7:
      portNum = 0x40; break;
    case 8:
      portNum = 0x80; break;
  }
  RegAddrConst ra;
  _transferSPI(ra.getOpcode(), ra.getAddrVal(gpio, 0), portNum);
}

void OutputConfig::setRegisterConfig(String addr, uint8_t data) { // mcp23S17t i/o expander spi transaction method for custom setups
  RegAddrConst ra;
  _transferSPI(ra.getOpcode(), ra.getAddrVal(addr, 0), data);
}

void OutputConfig::_transferSPI(uint8_t head, uint8_t addr, uint8_t data) { // mcp23S17t i/o expander's spi core transactions
  SPI.transfer(head); // send opcode byte
  SPI.transfer(addr); // send register address byte
  SPI.transfer(data); // send data byte:
}

void OutputConfig::transactMcpSPI(bool start) { // mcp23S17t i/o expanders enabling/disabling spi transactions
  if (start) {
    SPI.beginTransaction(_spiSet());    
  } else {
    SPI.endTransaction();    
  }
}

SPISettings OutputConfig::_spiSet() { // method configuring spi settings such as speeds, polarity, phase and data transfer orientation
  SPISettings spi(_speed, MSBFIRST, SPI_MODE0);
  return spi;
}

void OutputConfig::_displayMsg(String msg) { // displays the serial print outs to determine what happened along the process
  if (Serial && _msgState) {
    Serial.println(msg);  
  }
}



