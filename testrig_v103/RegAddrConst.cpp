#include "RegAddrConst.h"

#define OPCODE_WRITE 0x40
#define OPCODE_READ 0x41

String addrName[] = { // Referenced on MCP23S17T IOCON.BANK = 1
  "IODIRA",   "IODIRB",
  "IPOLA",    "IPOLB",
  "GPINTENA", "GPINTENB",
  "DEFVALA",  "DEFVALB",
  "INTCONA",  "INTCONB",
  "IOCON1",   "IOCON0",
  "GPPUA",    "GPPUB",
  "INTFA",    "INTFB",
  "INTCAPA",  "INTCAPB",
  "GPIOA",    "GPIOB",
  "OLATA",    "OLATB"
};

uint8_t addrValBank1[] = {  // Referenced on MCP23S17T IOCON.BANK = 1
  0x00,0x10,
  0x01,0x11,
  0x02,0x12,
  0x03,0x13,
  0x04,0x14,
  0x05,0x15,
  0x06,0x16,
  0x07,0x17,
  0x08,0x18,
  0x09,0x19,
  0x0A,0x1A
};

uint8_t addrValBank0[] = {  // Referenced on MCP23S17T IOCON.BANK = 0
  0x00,0x01,
  0x02,0x03,
  0x04,0x05,
  0x06,0x07,
  0x08,0x09,
  0x0A,0x1B,
  0x0C,0x1D,
  0x0E,0x1F,
  0x10,0x11,
  0x12,0x13,
  0x14,0x15
};

RegAddrConst::RegAddrConst(bool binVal) { // turns on the displaying of serial print outs
  _msgState = binVal;
  
}

String RegAddrConst::getAddrName(uint8_t index) { // gets the name of the register address by index
  return addrName[index];
}

uint8_t RegAddrConst::getOpcode(bool write) { // gets the hexadecimal value of the operation code (opcode) of the mcp23S17t i/o expander - it's like a header
  uint8_t operation = OPCODE_WRITE;
  if (!write) {
    operation = OPCODE_READ;
  }
  return operation;
}

uint8_t RegAddrConst::getAddrValBank1(uint8_t index) {  // gets the hexadecimal value of the register address setup as Bank1 declared by index
  return addrValBank1[index];
}

uint8_t RegAddrConst::getAddrValBank0(uint8_t index) {  // gets the hexadecimal value of the register address setup as Bank0 declared by index
  return addrValBank0[index];
}

uint8_t RegAddrConst::getAddrVal(String addr, bool bank) {  // gets the hexadecimal value of the register address declared by name for spi transaction 
  uint8_t regVal = 0xFF;
  for (int i = 0; i < sizeof(addrName); i++) {
    if (addr.equals(addrName[i])) {
      if (bank) {
        regVal = addrValBank1[i];
        break;
      }
      regVal = addrValBank0[i];
      break;
    }
  }

  return regVal;
}

void RegAddrConst::_displayMsg(String msg) {  // displays the serial print outs to determine what happened along the process
  if (Serial && _msgState) {
    Serial.println(msg);  
  }
}



