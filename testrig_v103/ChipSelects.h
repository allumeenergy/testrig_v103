#ifndef ChipSelects_H
#define ChipSelects_H

#if (ARDUINO >= 100)
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

class ChipSelects {

  public:
    //Variables
    
    //Constructor
    ChipSelects(bool binVal=false);
    
    //Methods
    void begin();
    void csMainDigiPot(uint8_t index);
    void csIsoDigiPot(uint8_t index);
    void csMainOutEn();
    void csIsoOutEn();
    void csMainPhaChg();
    void csMainResetMCPs();
    void csOff();
      
  private:
    //Variables
    bool _msgState;
    
    uint8_t _pins[5] = {
      A0, A1, A2, A3, 10
    };

    uint8_t _dmxDigiPotTable[10][4] {
      {0,0,0,0},
      {0,0,0,1},
      {0,0,1,0},
      {0,0,1,1},
      {0,1,0,0},
      {0,1,0,1},
      {0,1,1,0},
      {0,1,1,1},
      {1,0,0,0},
      {1,0,0,1},
    };
    
    //Methods
    void _displayMsg(String msg);
    void _displayMsg(String msg1, uint8_t index, String msg2);
    void _csDigiPotIndex(uint8_t index);
    void _csOutEnIndex();
  
};


#endif

