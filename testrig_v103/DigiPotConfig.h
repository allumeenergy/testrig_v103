#ifndef DigiPotConfig_H
#define DigiPotConfig_H

#include <SPI.h>
#include "ChipSelects.h"

#define WB_SETUP 0x00
#define WA_SETUP 0x01
#define Z_SETUP 0x02

#define MINI 0x00
#define MAXI 0x01

class DigiPotConfig {

  public:
    //Variables
    
    //Constructor
    DigiPotConfig(bool binVal=false);
 
    //Methods
    bool begin(ChipSelects cs, bool chkstate=false, uint32_t speed=SPI_CLOCK_DIV2);
    void transactDigiPotSPI(bool start);
    void setDefaultAll(uint8_t mode=WB_SETUP, uint8_t state=MINI); // 0 = WB setup, 1 = WA setup, 2 = Z-setup; 13.3mV = MINI, 133mV = MAXI
    void setWiperValueWB(uint8_t divisible);  // spi transfer WB setup
    void setWiperValueWA(uint8_t divisible);  // spi transfer WA setup
    void setWiperValue(uint8_t divisible);    // spi transfer for a different circuit layout Z-setup
      
  private:
    //Variables
    bool _msgState;
    uint32_t _speed;
        
    //Methods
    void _displayMsg(String msg);
    SPISettings _spiSet();
    ChipSelects _cs;
   
};


#endif

