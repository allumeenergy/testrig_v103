#include "RelaySignals.h"

RelaySignals::RelaySignals(bool binVal) { // turns on the displaying of serial print outs
  _msgState = binVal;
  
}

void RelaySignals::begin() {  // setup arduino pins to be used for the relay signals (rs)
  
  _displayMsg("...RS Init!");
  
  for (uint8_t i = 0; i < sizeof(_pins); i++) {
    pinMode(_pins[i], INPUT);
  }
  _displayMsg("...10 RS Inputs: Active!");   
  
}

void RelaySignals::_displayMsg(String msg) {  // displays the serial print outs to determine what happened along the process
  if (Serial && _msgState) {
    Serial.println(msg);  
  }
}

bool RelaySignals::isOnRelay(uint8_t index) { // determines the status of the arduino pin declared (if relay signals are found on the declared pin)
  if (index > 10 || index < 0) {
    index = 1;
  }
  return digitalRead(_pins[index-1]);
}

