#include "RelaySignals.h"
#include "ChipSelects.h"
#include "DigiPotConfig.h"
#include "OutputConfig.h"

RelaySignals rs;    //set relaySignals(true) to display message;
ChipSelects cs;     //set chipSelects(true) to display message;
DigiPotConfig dpc;  //set DigiPotConfig(true) to display message; 
OutputConfig opc;   //set OutputConfig(true) to display message;

uint8_t testState = 2; // 0 = Normal Scenario Test; 1 = Undesired Scenario Test; 2 = No Test

void setup() {
  Serial.begin(9600);
  Serial.println("Start Setup.");
  
  rs.begin();
  cs.begin();
  opc.begin(cs, dpc.begin(cs));     //setup SPI configs on digipot and i/o expanders (MCPs)
  dpc.setDefaultAll();              //setup default values of digital potentiometers, WB setup by default
                                    //setup default states of i/o expanders

  Serial.println("End Setup."); Serial.println();

}

void loop() {

  // RelaySignals Methods
  //  - rs.begin() = instantiates all the operations (e.g. pin setups) to be used for the relay signal inputs.
  //  - rs.isOnRelay(relayNumber) = returns boolean state of the specified relay among 10 units.
  
  // ChipSelects Methods
  //  * the following enables the chip select of the specified:
  //  - cs.begin() = instantiates all the operations (e.g. pin setups) to be used for the cs signal inputs for the SPI devices (e.g. digipot, i/o expanders).
  //  - cs.csMainDigiPot(potentiometer to control) = mainboard digital potentiometer to be configure.
  //  - cs.csIsoDigiPot(potentiometer to control) = isolated board digital potentiometer to be configure.
  //  - cs.csMainOutEn() = mainboard output enable i/o expander e.g. MCP23S17T-E/SO, i/o expanders determine w/c mainboard output number is enable after spi transact
  //  - cs.csIsoOutEn() = isolated board output enable i/o expander e.g. MCP23S17T-E/SO, i/o expanders determine w/c isolated output number is enable after spi transact
  //  - cs.csMainPhaChg() = mainboard output phase change or phase reversal e.g. MCP23S17T-E/SO, i/o expanders determine w/c mainboard output number is change after spi transact
  //  - cs.csMainResetMCPs() = resets all the i/o expanders e.g. MCP23S17T-E/SO
  //  - cs.csOff() = turn off all chip selects (no selected SPI device)

  // DigiPotConfig Methods
  //  - dpc.begin(bool spiStartedAlready, speed) = instantiates SPI connection based if the SPI is already instantiated as well as specifying SPI speed
  //  - dpc.transactDigiPotSPI(bool start/end) = calls out the begin or end transaction on SPI bus based on settings on the DigiPotConfig instantiated class.
  //  - dpc.setWiperValueWB(index) = sends out data to set digipot to a resistance that is specified by indeces starting from 1 (133mV) to 10 (13.3mV)
  //  - dpc.setDefaultAll(mode, voltage) = set all digital potentiometers to a specific resistance value of either all will have 13.3mV / 133mV as default voltage output
  
  // OutputConfig Methods
  //  - opc.begin(bool spiStartedAlready, speed) = instantiates SPI connection based if the SPI is already instantiated as well as specifying SPI speed
  //  - opc.transactMcpSPI(bool start/end) = calls out the begin or end transaction on SPI bus based on settings on the DigiPotConfig instantiated class.
  //  - opc.setRegisterConfig(addr, value) = sends out data to set specific command to an address of i/o expanders (e.g. MCP23S17T single cast multiple gpio config).
  //  - opc.setEnableOutPort(i/o port) = sends out data to set an i/o port active driving analog switches 

  // *********Testing Menu Procedures:
  
  if (testState == 2) { // test scenario selection
    Serial.println("Test Mode: 0,1 : Normal,Undesired");
    String cmd = "";
    while (testState == 2) {
      while (Serial.available() != 0) {
        char c = Serial.read();
        cmd += c;
        delay(10);
      }
      
      if (cmd.equals("0")) {
        testState = 0; break;
        
      } else if (cmd.equals("1")) {
        testState = 1; break;
        
      } else if (!cmd.equals("")) {
        Serial.println("..wrong input!");
        
      } cmd = "";
    }
  } Serial.println();

  if (testState == 0) { // for normal test scenario algorithm
    Serial.println("Normal Test Chosen!");
    Serial.print("Number of Unit(s) to test: ");

    while (testState == 0) {
    
      uint8_t val = readInput();
      Serial.println(val);
      
      uint8_t units[val];
      uint8_t unitCtr = 0;

      if (val == 100) {
          //do nothing
      } else if (val > 0 && val <= 10) {
          processNormalTest(units, unitCtr, val);
          Serial.print("Input Unit # to test: ");  // input how many units to be tested
      } else {
          Serial.println("..not a number / out of range !");        
      }
      
    }    
  } Serial.println();

  if (testState == 1) { // for undesired test scenario algorithm
    
  } Serial.println();
}


// methods w/in main program of test rig
void processNormalTest(uint8_t units[], uint8_t unitCtr, uint8_t val) {
  while (unitCtr != val) {
    Serial.print("Test Unit#: "); // input units to be tested
    while (1) {
      uint8_t num = readInput();
      if (num == 200) {
        Serial.println("..not number / out of range !");
      } else if (num != 100) {
        units[unitCtr] = num;
        unitCtr++;
        break;
      }
    }
  }
  
  // start digipot spi transactions
  dpc.transactDigiPotSPI(1);
  for (uint8_t i = 1; i <= 10; i++) {  // reset all digipot voltage values to 13.3mV
    cs.csMainDigiPot(i);
    dpc.setWiperValueWB(10);
    cs.csOff();
    delay(2);
  }          
  for (uint8_t i = 1; i <= sizeof(units); i++) { // turn all input digipot voltages to 133mV or lesser based on # of units to test (divisible)
    cs.csMainDigiPot(units[i]);
    dpc.setWiperValueWB(val);
    cs.csOff();
    delay(2);
  }
  dpc.transactDigiPotSPI(0);
  
  // start i/o expander spi transactions
  opc.transactMcpSPI(1);
  cs.csMainOutEn(); // turn off all i/o ports on mainboard
  opc.setRegisterConfig("GPIOA", 0x00);
  cs.csOff(); delay(2);
  cs.csMainOutEn();
  opc.setRegisterConfig("GPIOB", 0x00);
  cs.csOff(); delay(2);
  
  cs.csIsoOutEn(); // turn off all i/o ports on isolated board
  opc.setRegisterConfig("GPIOA", 0x00);
  cs.csOff(); delay(2);
  cs.csIsoOutEn();
  opc.setRegisterConfig("GPIOB", 0x00);
  cs.csOff(); delay(2);
  
  for (uint8_t i = 1; i <= val; i++) { // turn on all chosen i/o ports
    cs.csMainOutEn();
    opc.setEnableOutPort(units[i]);
    cs.csOff(); delay(2);
  
    cs.csIsoOutEn();
    opc.setEnableOutPort(units[i]);
    cs.csOff(); delay(2);
  }          
  opc.transactMcpSPI(0);
  
  Serial.print("OutEN Unit(s)#: ");
  String fill = " ";
  for (uint8_t i = 0; i < sizeof(units); i++) {
    Serial.print(units[i]); Serial.print(fill);
  } Serial.println();

}

uint8_t readInput() { // read serial input and identify if input is a valid digit that will be used inside either normal or undesired test algorithm
  String input = "";
  while (Serial.available() != 0) {
    char c = Serial.read();
    input += c;
    delay(10);
  }
  uint8_t num = 100;
  if (isNumeric(input)) {
    num = input.toInt();
  } else if (input.equals("")) {
    num = 100;
  } else {
    num = 200;
  } 
  return num;
}

boolean isNumeric(String str) { // determine if input is a digit or not
    unsigned int stringLength = str.length();
 
    if (stringLength == 0) {
        return false;
    }
 
    boolean seenDecimal = false;
 
    for(unsigned int i = 0; i < stringLength; ++i) {
        if (isDigit(str.charAt(i))) {
            continue;
        }
 
        if (str.charAt(i) == '.') {
            if (seenDecimal) {
                return false;
            }
            seenDecimal = true;
            continue;
        }
        return false;
    }
    return true;
}




