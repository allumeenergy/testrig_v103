#include "ChipSelects.h"

ChipSelects::ChipSelects(bool binVal) {  // turns on the displaying of serial print outs
  _msgState = binVal;
  
}

void ChipSelects::begin() { // setup all pins to be used for chip select on dmxs.
  
  _displayMsg("...CS Init!");
  
  for (uint8_t i = 0; i < sizeof(_pins); i++) {
    pinMode(_pins[i], INPUT);
  }
  csOff();
  _displayMsg("...CS: Active!");
  
}

void ChipSelects::_displayMsg(String msg) { // displays the serial print outs to determine what happened along the process
  if (Serial && _msgState) {
    Serial.println(msg);  
  }
}

void ChipSelects::_displayMsg(String msg1, uint8_t index, String msg2) { // displays the serial print outs to determine what happened along the process
  if (Serial && _msgState) {
    Serial.print(msg1);
    Serial.print(index);
    Serial.println(msg2);  
  }
}

void ChipSelects::csOff() { // turn of chip select
  digitalWrite(_pins[4], HIGH);
  digitalWrite(_pins[0], 1);
  digitalWrite(_pins[1], 1);
  digitalWrite(_pins[2], 1);
  digitalWrite(_pins[3], 0);
  _displayMsg("..CS Off!"); 
}

void ChipSelects::csMainDigiPot(uint8_t index) {  // turn on chip select of declared digipot on the main board
  digitalWrite(_pins[4], LOW);
  if (index > 10 || index <= 0) {
    index = 1;
  }
  _csDigiPotIndex(index);
  _displayMsg("..Pick MainDigiPot#", index, "!");
}

void ChipSelects::csIsoDigiPot(uint8_t index) {  // turn on chip select of declared digipot on the isoalted board
  digitalWrite(_pins[4], HIGH);
  if (index > 10 || index <= 0) {
    index = 1;
  }
  _csDigiPotIndex(index);
  _displayMsg("..Pick IsoDigiPot#", index, "!");  
}

void ChipSelects::csMainOutEn() {  // turn on chip select of declared i/o expander on the main board
  digitalWrite(_pins[4], LOW);
  _csOutEnIndex();
  _displayMsg("..Pick MainOutEnable!");  
}

void ChipSelects::csIsoOutEn() {  // turn on chip select of declared i/o expander on the isolated board
  digitalWrite(_pins[4], HIGH);
  _csOutEnIndex();
  _displayMsg("..Pick IsoOutEnable!");   
}

void ChipSelects::csMainPhaChg() {  // turn on chip select of declared output phase change on the main board
  digitalWrite(_pins[4], LOW);
  digitalWrite(_pins[0], 1);
  digitalWrite(_pins[1], 1);
  digitalWrite(_pins[2], 0);
  digitalWrite(_pins[3], 0);
  _displayMsg("..Pick Mainboard PhaseChange!");   
}

void ChipSelects::csMainResetMCPs() {  // reset all i/o expanders (output and phase changer i/o expander)
  digitalWrite(_pins[4], LOW);
  digitalWrite(_pins[0], 1);
  digitalWrite(_pins[1], 1);
  digitalWrite(_pins[2], 1);
  digitalWrite(_pins[3], 1);
  _displayMsg("..Reset All OutEnables+PhaseChange!");   
}

void ChipSelects::_csDigiPotIndex(uint8_t index) {  // algorithm to chip select digital potentiometer via dmx
  for (uint8_t i = 0; i < 4; i++) {
    digitalWrite(_pins[i], _dmxDigiPotTable[index-1][i]);
  }
}

void ChipSelects::_csOutEnIndex() { // algorithm to chip select i/o expander via dmx
  digitalWrite(_pins[0], 1);
  digitalWrite(_pins[1], 0);
  digitalWrite(_pins[2], 1);
  digitalWrite(_pins[3], 0);
}

