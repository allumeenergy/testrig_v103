#ifndef RegAddrConst_H
#define RegAddrConst_H

#if (ARDUINO >= 100)
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif

class RegAddrConst {

  public:
    //Variables
    
    //Constructor
    RegAddrConst(bool binVal=false);

    //Methods
    String getAddrName(uint8_t index);
    uint8_t getAddrValBank1(uint8_t index);
    uint8_t getAddrValBank0(uint8_t index);
    uint8_t getAddrVal(String addr, bool bank);
    uint8_t getOpcode(bool write=false);
  
  private:
    //Variables
    bool _msgState;
    
    //Methods
    void _displayMsg(String msg);
    
};


#endif

